FROM ubuntu:trusty

RUN  DEBIAN_FRONTEND=noninteractive apt-get -qy update && apt-get -y install \
	bc \
	build-essential \
	cpio \
	curl \
	git \
	openssl \
	python \
	sudo \
	unzip \
	wget \
    rsync \
	nodejs \
	node-gyp \
	nodejs-dev \
    pv \
	npm \
	jq \
	apt-transport-https \
    ca-certificates \
    curl \
    gnupg2 \
    software-properties-common \
	npm \
	upx-ucl \
	coreutils \
	realpath

RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - && \
	add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" && apt-get update && \
   apt-get -y install docker-ce

RUN cd /tmp && wget https://dl.google.com/go/go1.11.11.linux-amd64.tar.gz && \
	tar xaf go1.11.11.linux-amd64.tar.gz && mv go /usr/local && rm -f go1.11.11.linux-amd64.tar.gz

RUN npm config set strict-ssl false && npm install -g bower

RUN rm -rf /var/lib/apt/lists/*

ENV GOROOT /usr/local/go

